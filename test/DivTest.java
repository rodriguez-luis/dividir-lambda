import org.junit.Assert;
import org.junit.Test;
public class DivTest {
    @Test
    public void testDividir(){
        Div div = new Div();
        Assert.assertEquals(2, div.dividir(8, 4), 1);
    }
    @Test
    public void testDividirLambda(){
        Div div = new Div();
        Assert.assertEquals(2, div.dividirLambda(10, 5), 1);
    }
}
