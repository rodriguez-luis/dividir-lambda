public class Div {
    float dividir(int a, int b) throws ArithmeticException{
        try {
            return a/(float)b;
        }
        catch (Exception e){
            return a < 0 ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
        }
    }

    public float operadorBinario(DivInterface div, int a, int b){
       return div.dividir(a , b);
    }
    float dividirLambda(int a, int b){
        return operadorBinario((c, d) -> a /(float)b, a, b);
    }
}
